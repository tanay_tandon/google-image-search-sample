package com.example.tanaytandon.searchsample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.tanaytandon.searchsample.R;
import com.example.tanaytandon.searchsample.constants.IntentActions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ColumnDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "ColumnDIalog";

    public interface Keys {
        String COLUMN_COUNT = "columnCount";
    }

    public static ColumnDialog show(FragmentManager fragmentManager, int columnCount) {
        Bundle args = new Bundle();
        args.putInt(Keys.COLUMN_COUNT, columnCount);
        ColumnDialog fragment = new ColumnDialog();
        fragment.setArguments(args);
        fragment.show(fragmentManager, TAG);
        return fragment;
    }

    @BindView(R.id.columnGroup)
    RadioGroup columnRadioGroup;

    @BindView(R.id.submit)
    View submitBtn;

    private int columnCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            columnCount = args.getInt(Keys.COLUMN_COUNT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.column_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        submitBtn.setOnClickListener(this);

        int id = R.id.two;

        switch (columnCount) {
            case 3: {
                id = R.id.three;
                break;
            }

            case 4: {
                id = R.id.four;
                break;
            }
        }

        columnRadioGroup.check(id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit: {
                sendBroadcast();
                break;
            }
        }
    }

    private void sendBroadcast() {
        int selectedId = columnRadioGroup.getCheckedRadioButtonId();
        int columnCount = 2;
        switch (selectedId) {
            case R.id.three: {
                columnCount = 3;
                break;
            }

            case R.id.four: {
                columnCount = 4;
                break;
            }
        }

        Intent intent = new Intent(IntentActions.COLUMN_COUNT);
        intent.putExtra(Keys.COLUMN_COUNT, columnCount);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        dismiss();
    }

}
