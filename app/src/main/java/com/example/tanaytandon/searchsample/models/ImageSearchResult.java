package com.example.tanaytandon.searchsample.models;

import java.util.ArrayList;

public class ImageSearchResult {

    public String query;

    public ArrayList<ImageSearchResultItem> items;
}
