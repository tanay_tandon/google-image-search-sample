package com.example.tanaytandon.searchsample.models;

import com.orm.dsl.Table;

@Table
public class SearchQuery {

    public String query;

    public Long timestamp;

    public Long id;
}
