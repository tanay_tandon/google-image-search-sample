package com.example.tanaytandon.searchsample.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.example.tanaytandon.searchsample.ui.ColumnDialog;
import com.example.tanaytandon.searchsample.ui.ImageSearch;
import com.example.tanaytandon.searchsample.R;
import com.example.tanaytandon.searchsample.ui.SearchActivity;
import com.example.tanaytandon.searchsample.ui.SearchFragment;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.example.tanaytandon.searchsample.ui.ImageSearch.TAG;

public class FirebaseService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        buildNotification(remoteMessage);
    }

    private void buildNotification(RemoteMessage remoteMessage) {
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            Intent intent;
            String action = notification.getClickAction();
            if (action != null) {
                intent = new Intent(action);
            } else {
                intent = new Intent(this, SearchActivity.class);
            }

            if (remoteMessage.getData() != null) {
                String query = remoteMessage.getData().get(SearchFragment.Keys.QUERY);
                if (!TextUtils.isEmpty(query)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(SearchFragment.Keys.QUERY, query);
                    intent.putExtras(bundle);
                }
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, ImageSearch.NotificationChannels.IMAGE_SEARCH)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(notification.getBody()))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(notification.getTitle())
                    .setContentText(notification.getBody())
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(ImageSearch.getInstance().getNotificationId(), builder.build());
            }
        }
    }

    public void onNewToken(String token) {
        Log.d(TAG, "onNewToken: token is " + token);
    }


}
