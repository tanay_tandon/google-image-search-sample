package com.example.tanaytandon.searchsample.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.tanaytandon.searchsample.R;
import com.example.tanaytandon.searchsample.models.ImageSearchResultItem;
import com.example.tanaytandon.searchsample.utils.DeviceUtils;
import com.example.tanaytandon.searchsample.utils.PixelUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.ImageGridItemHolder> {

    private Context context;

    private ArrayList<ImageSearchResultItem> items;

    private LayoutInflater layoutInflater;

    private View.OnClickListener clickListener;

    private int columnCount;

    public ImageGridAdapter(Context context, ArrayList<ImageSearchResultItem> items,
                            View.OnClickListener clickListener, int columnCount) {
        this.context = context;
        this.items = items;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.clickListener = clickListener;
        this.columnCount = columnCount;
    }

    @NonNull
    @Override
    public ImageGridItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.item_search_result, viewGroup, false);
        return new ImageGridItemHolder(view, clickListener, columnCount);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGridItemHolder imageGridItemHolder, int i) {
        ImageSearchResultItem item = items.get(i);
        imageGridItemHolder.bind(item, i);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public static class ImageGridItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;

        @BindView(R.id.rootView)
        View rootView;

        private Context context;

        private int width;

        private int height;

        ImageGridItemHolder(@NonNull View itemView, View.OnClickListener clickListener, int columnCount) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rootView.setOnClickListener(clickListener);
            width = PixelUtils.dpToPix(184);
            height = PixelUtils.dpToPix(184);
            this.context = itemView.getContext();
        }

        void bind(ImageSearchResultItem imageSearchResult, int position) {
            Picasso.get().load(imageSearchResult.link).centerCrop().resize(width, height).into(imageView);
            rootView.setTag(position);
        }

    }
}
