package com.example.tanaytandon.searchsample.models;

public class ViewState<T> {

  public   interface Type {
        int CONTENT = 1;
        int ERROR = 2;
        int NO_INTERNET = 3;
    }

    public T data;

    public NetworkError networkError;

    public int type;
}
