package com.example.tanaytandon.searchsample.data.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


public class NetworkManager {
    private static NetworkManager instance;
    private RequestQueue requestQueue;
    private AtomicInteger atomicInteger;

    private NetworkManager(Context context) {
        this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        this.atomicInteger = new AtomicInteger(10);
    }

    public synchronized static NetworkManager newInstance(Context context) {

        if (instance == null) {
            instance = new NetworkManager(context.getApplicationContext());
        }

        return instance;
    }

    private RequestQueue getRequestQueue() {
        return requestQueue;
    }

    private void addToRequestQueue(Request request) {
        getRequestQueue().add(request);
    }

    void cancelRequest(Object tag) {
        getRequestQueue().cancelAll(tag);
    }


    public <T> void get(String url, Map<String, String> headers, final NetworkInteractionListener<T> networkInteractionListener,
                        Class<T> clazz, boolean isResponseList, Object tag) {

        addToRequestQueue(makeRequest(url, null, headers, Request.Method.GET, networkInteractionListener,
                clazz, isResponseList, tag));
    }

    public <T> void post(String url, String body, Map<String, String> headers, final NetworkInteractionListener<T> networkInteractionListener,
                         Class<T> clazz, boolean isResponseList, Object tag) {
        addToRequestQueue(makeRequest(url, body, headers, Request.Method.POST, networkInteractionListener,
                clazz, isResponseList, tag));
    }


    private <T> JsonRequest<T> makeRequest(String url, String body, final Map<String, String> headers, int requestType, final NetworkInteractionListener<T> networkInteractionListener,
                                           final Class<T> clazz, final boolean isResponseList, Object tag) {

        if (tag == null) {
            tag = atomicInteger.incrementAndGet();
        }

        Response.Listener<T> responseListener = new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                networkInteractionListener.onSuccess(response);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                networkInteractionListener.onFail(ResponseHelper.getNetworkErrorFromVolleyError(error));
            }
        };

        JsonRequest<T> request = new JsonRequest<T>(requestType, url, body, responseListener, errorListener) {
            @Override
            protected Response<T> parseNetworkResponse(NetworkResponse response) {
                return ResponseHelper.parseModel(response, getType(clazz, isResponseList));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        request.setTag(tag);
        return request;
    }

    private static <T> Type getType(Class<T> clazz, boolean isResponseList) {
        return isResponseList ? TypeToken.getParameterized(ArrayList.class, clazz).getType() : TypeToken.get(clazz).getType();
    }


}
