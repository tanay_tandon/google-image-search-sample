package com.example.tanaytandon.searchsample.models;

public class DeviceDimensions {

    public int height;

    public int width;

    public static DeviceDimensions newInstance(int height, int width) {
        DeviceDimensions deviceDimensions = new DeviceDimensions();
        deviceDimensions.height = height;
        deviceDimensions.width = width;
        return deviceDimensions;
    }
}
