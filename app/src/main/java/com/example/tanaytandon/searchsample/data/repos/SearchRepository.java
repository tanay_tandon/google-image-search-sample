package com.example.tanaytandon.searchsample.data.repos;

import com.example.tanaytandon.searchsample.data.network.ApiUrl;
import com.example.tanaytandon.searchsample.data.network.NetworkInteractionListener;
import com.example.tanaytandon.searchsample.models.ImageSearchResult;
import com.example.tanaytandon.searchsample.models.NetworkError;
import com.example.tanaytandon.searchsample.models.ViewState;
import com.example.tanaytandon.searchsample.utils.DeviceUtils;

import java.util.Collections;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class SearchRepository extends Repo {

    private static SearchRepository instance;

    private static String GOOGLE_SEARCH_KEY = "AIzaSyBtbi2pZClq-hPJHM1f6NbneItSnCIPvNQ";
    private static String GOOGLE_SEARCH_ENGINE_NAME = "002314014913363730003:3afemrjfqq4";

    private static String BASE_SEARCH_URL = ApiUrl.SEARCH_URL + "?key=" + GOOGLE_SEARCH_KEY + "&cx=" + GOOGLE_SEARCH_ENGINE_NAME + "&searchType=image";

    public synchronized static SearchRepository getInstance() {
        if (instance == null) {
            instance = new SearchRepository();
        }
        return instance;
    }

    private PublishSubject<ViewState<ImageSearchResult>> publishSubject;

    private SearchRepository() {
        publishSubject = PublishSubject.create();
    }

    public Observable<ViewState<ImageSearchResult>> getImageSearchResultObservable() {
        return publishSubject;
    }

    public void searchImages(final String query, int itemSize) {
        String url = BASE_SEARCH_URL + "&q=" + query + (itemSize == 0 ? "" : "&start=" + itemSize);
        if (DeviceUtils.newInstance().isDeviceConnected()) {
            getNetworkManager().get(url, Collections.<String, String>emptyMap(), new NetworkInteractionListener<ImageSearchResult>() {
                @Override
                public void onSuccess(ImageSearchResult result) {
                    result.query = query;
                    sendData(publishSubject, result);
                }

                @Override
                public void onFail(NetworkError networkError) {
                    sendNetworkError(publishSubject, networkError);
                }
            }, ImageSearchResult.class, false, 0);
        } else {
            sendNoInternet(publishSubject);
        }
    }


}
