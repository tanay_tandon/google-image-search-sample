package com.example.tanaytandon.searchsample.ui;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.orm.SugarApp;

import java.util.concurrent.atomic.AtomicInteger;

public class ImageSearch extends SugarApp {

    public static final String TAG = "ImageSearch";

    private static ImageSearch instance;

    private AtomicInteger notificationAtomicInteger;

    public interface NotificationChannels {
        String IMAGE_SEARCH = "imageSearch";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        notificationAtomicInteger = new AtomicInteger(10);
    }

    public static ImageSearch getInstance() {
        return instance;
    }

    public void setupFirebaseMessaging() {
        registerFirebaseToken();
        registerForTopic();
    }

    public void registerFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                try {
                    Log.d(TAG, "onComplete: token is " + task.getResult().getToken());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void registerForTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("sample").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "onComplete: subscribed");
                } else {
                    Log.d(TAG, "onComplete: failure while subscribing");
                }
            }
        });
    }

    public int getNotificationId() {
        return notificationAtomicInteger.incrementAndGet();
    }

}

