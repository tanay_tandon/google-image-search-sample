package com.example.tanaytandon.searchsample.data.network;

import com.example.tanaytandon.searchsample.models.NetworkError;

public interface NetworkInteractionListener<T> {

    void onSuccess(T result);

    void onFail(NetworkError networkError);
}

