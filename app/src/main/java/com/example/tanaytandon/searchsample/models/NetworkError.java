package com.example.tanaytandon.searchsample.models;

public class NetworkError {
    public String message;

    public int statusCode;

    private NetworkError() {
    }

    public static NetworkError newInstance(String message, int statusCode) {
        NetworkError networkError = new NetworkError();
        networkError.message = message;
        networkError.statusCode = statusCode;
        return networkError;
    }

}
