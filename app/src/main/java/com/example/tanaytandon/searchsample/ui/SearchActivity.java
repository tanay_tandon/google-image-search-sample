package com.example.tanaytandon.searchsample.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tanaytandon.searchsample.R;
import com.example.tanaytandon.searchsample.constants.IntentActions;

public class SearchActivity extends AppCompatActivity {

    private int columnCount = 2;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && IntentActions.COLUMN_COUNT.equalsIgnoreCase(intent.getAction())) {
                columnCount = intent.getIntExtra(ColumnDialog.Keys.COLUMN_COUNT, 2);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        if (savedInstanceState == null) {
            Intent intent = getIntent();

            String query = (intent != null && intent.getExtras() != null) ? intent.getExtras().getString(SearchFragment.Keys.QUERY) : null;
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, TextUtils.isEmpty(query) ? SearchFragment.newInstance() : SearchFragment.newInstance(query))
                    .commitNow();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(IntentActions.COLUMN_COUNT));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.columnCount: {
                showColumnDialog();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showColumnDialog() {
        ColumnDialog.show(getSupportFragmentManager(), columnCount);
    }
}
