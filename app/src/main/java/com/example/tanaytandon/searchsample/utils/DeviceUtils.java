package com.example.tanaytandon.searchsample.utils;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

import com.example.tanaytandon.searchsample.ui.ImageSearch;
import com.example.tanaytandon.searchsample.models.DeviceDimensions;

import static android.content.Context.WINDOW_SERVICE;

public class DeviceUtils {

    private static DeviceUtils instance;

    private DeviceDimensions deviceDimensions;

    private DeviceUtils() {

    }

    public static DeviceUtils newInstance() {
        if (instance == null) {
            instance = new DeviceUtils();
        }
        return instance;
    }


    public boolean isDeviceConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) ImageSearch.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                for (Network network : connectivityManager.getAllNetworks()) {
                    NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                    if (networkInfo.isConnected()) {
                        return true;
                    }
                }
            } else {
                for (NetworkInfo networkInfo : connectivityManager.getAllNetworkInfo()) {
                    if (networkInfo.isConnected()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public DeviceDimensions getDeviceDimensions() {
        if (deviceDimensions == null) {
            WindowManager windowManager = (WindowManager) ImageSearch.getInstance().getSystemService(WINDOW_SERVICE);
            if (windowManager != null) {
                Display display = windowManager.getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                deviceDimensions = DeviceDimensions.newInstance(point.y, point.x);
            } else {
                deviceDimensions = DeviceDimensions.newInstance(PixelUtils.dpToPix(640), PixelUtils.dpToPix(480));
            }
        }
        return deviceDimensions;
    }
}



