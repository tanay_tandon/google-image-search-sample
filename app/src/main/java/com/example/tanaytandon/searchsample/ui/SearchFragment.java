package com.example.tanaytandon.searchsample.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tanaytandon.searchsample.R;
import com.example.tanaytandon.searchsample.constants.IntentActions;
import com.example.tanaytandon.searchsample.data.repos.SearchRepository;
import com.example.tanaytandon.searchsample.models.ImageSearchResult;
import com.example.tanaytandon.searchsample.models.ImageSearchResultItem;
import com.example.tanaytandon.searchsample.models.ViewState;
import com.example.tanaytandon.searchsample.utils.PixelUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class SearchFragment extends Fragment implements View.OnClickListener {

public     interface Keys {
        String QUERY = "query";
    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    public static SearchFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(Keys.QUERY, query);
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.editText)
    EditText searchEditText;

    @BindView(R.id.button)
    Button searchButton;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    View progressBar;

    private Disposable disposable;
    private ImageGridAdapter adapter;
    private ArrayList<ImageSearchResultItem> items;
    private String query;
    private boolean isLoading;
    private int columnCount = 2;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && IntentActions.COLUMN_COUNT.equalsIgnoreCase(intent.getAction())) {
                columnCount = intent.getIntExtra(ColumnDialog.Keys.COLUMN_COUNT, 2);
                changeColumnCount();
            }
        }
    };


    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.d(ImageSearch.TAG, "onScrolled: dx and dy are " + dx + " ,  " + dy);
            int totalVisibleItems = recyclerView.getChildCount();
            int totalItems = recyclerView.getLayoutManager().getItemCount();
            int firstVisibleItemPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            if ((totalItems - totalVisibleItems) <= firstVisibleItemPosition && dy > 0) {
                mayLoadMore();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            query = args.getString(Keys.QUERY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,
                new IntentFilter(IntentActions.COLUMN_COUNT));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchButton.setOnClickListener(this);
        setupRecyclerView();
        ImageSearch.getInstance().setupFirebaseMessaging();
        if (!TextUtils.isEmpty(query)) {
            searchEditText.setText(query);
            searchForImages();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button: {
                searchForImages();
                break;
            }
        }
    }

    private void searchForImages() {
        query = searchEditText.getText().toString();
        items.clear();
        isLoading = true;
        search(query);
    }

    private void search(String query) {
        progressBar.setVisibility(View.VISIBLE);
        searchButton.setEnabled(false);
        disposable = SearchRepository.getInstance().getImageSearchResultObservable().subscribe(this::onStateChange, this::onError);
        SearchRepository.getInstance().searchImages(query, items.size());
    }


    private void setupRecyclerView() {
        items = new ArrayList<>();
        adapter = new ImageGridAdapter(getContext(), items, this, columnCount);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), columnCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(onScrollListener);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                int dp8 = PixelUtils.dpToPix(8);
                outRect.left = dp8;
                outRect.right = dp8;
                outRect.top = dp8;
                outRect.bottom = dp8;
            }
        });
    }

    private void onStateChange(ViewState<ImageSearchResult> viewState) {
        switch (viewState.type) {
            case ViewState.Type.CONTENT: {
                addItems(viewState.data);
                break;
            }

            case ViewState.Type.ERROR: {
                showErrorToast();
                hideProgressBarAndEnableSearchBtn();
                break;
            }

            case ViewState.Type.NO_INTERNET: {
                showNoInternetToast();
                hideProgressBarAndEnableSearchBtn();
                break;
            }
        }
    }

    private void showErrorToast() {
        Toast.makeText(getContext(), R.string.serverError, Toast.LENGTH_LONG).show();
    }

    private void showNoInternetToast() {
        Toast.makeText(getContext(), R.string.noInternet, Toast.LENGTH_LONG).show();
    }

    private void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    private void addItems(ImageSearchResult imageSearchResult) {
        items.addAll(imageSearchResult.items);
        adapter.notifyDataSetChanged();
        dispose(disposable);
        isLoading = false;
        hideProgressBarAndEnableSearchBtn();
    }

    private void hideProgressBarAndEnableSearchBtn() {
        progressBar.setVisibility(View.GONE);
        searchButton.setEnabled(true);
    }

    private void dispose(Disposable disposable) {
        if (disposable != null && disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    private void mayLoadMore() {
        if (!isLoading && items.size() < 100) {
            isLoading = true;
            search(query);
        } else if (items.size() == 100) {
            Toast.makeText(getContext(), R.string.reachedEndOfList, Toast.LENGTH_SHORT).show();
        }
    }

    private void changeColumnCount() {
        GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        gridLayoutManager.setSpanCount(columnCount);
        adapter.setColumnCount(columnCount);
        adapter.notifyDataSetChanged();
    }
}
