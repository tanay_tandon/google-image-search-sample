package com.example.tanaytandon.searchsample.models;

import com.orm.dsl.Table;

@Table
public class ImageSearchResultItem {

    public Long id;

    public String link;

    public Long searchQueryId;
}
