package com.example.tanaytandon.searchsample.data.network;

public interface ApiUrl {

    String SEARCH_URL = "https://www.googleapis.com/customsearch/v1";
}
