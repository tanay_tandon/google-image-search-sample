package com.example.tanaytandon.searchsample.utils;

import android.util.TypedValue;

import com.example.tanaytandon.searchsample.ui.ImageSearch;

public class PixelUtils {

    public static int dpToPix(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                ImageSearch.getInstance().getResources().getDisplayMetrics());

    }

}
