package com.example.tanaytandon.searchsample.data.repos;

import com.example.tanaytandon.searchsample.ui.ImageSearch;
import com.example.tanaytandon.searchsample.data.network.NetworkManager;
import com.example.tanaytandon.searchsample.models.NetworkError;
import com.example.tanaytandon.searchsample.models.ViewState;

import io.reactivex.Observer;

abstract class Repo {

    private NetworkManager networkManager;

    Repo() {
        networkManager = NetworkManager.newInstance(ImageSearch.getInstance());
    }

    NetworkManager getNetworkManager() {
        return networkManager;
    }

    <T> void sendNoInternet(Observer<ViewState<T>> observer) {
        ViewState<T> viewState = new ViewState<>();
        viewState.type = ViewState.Type.NO_INTERNET;
        observer.onNext(viewState);
    }

    <T> void sendNetworkError(Observer<ViewState<T>> observer, NetworkError networkError) {
        ViewState<T> viewState = new ViewState<>();
        viewState.type = ViewState.Type.NO_INTERNET;
        observer.onNext(viewState);
    }

    <T> void sendData(Observer<ViewState<T>> observer, T data) {
        ViewState<T> viewState = new ViewState<>();
        viewState.data = data;
        viewState.type = ViewState.Type.CONTENT;
        observer.onNext(viewState);
    }
}
